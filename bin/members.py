#!/usr/bin/python
import ldap, time, operator

#
# globals
#
cscUri = "http://csclub.uwaterloo.ca/xsltproc"
cscTerms = ["Winter", "Spring", "Fall"]
cscShortTerms = ['w', 's', 'f']
cscLdapUri = "ldap://ldap1.csclub.uwaterloo.ca ldap://ldap2.csclub.uwaterloo.ca"
cscLdap = None
cscPeopleBase = 'ou=People,dc=csclub,dc=uwaterloo,dc=ca'

def cscLdapConnect():
    global cscLdap
    cscLdap = ldap.initialize(cscLdapUri)
    cscLdap.simple_bind_s("", "")

cscLdapConnect()
curDate = time.strftime('%d-%m-%Y')
year = time.localtime().tm_year
term = cscShortTerms[int(time.localtime().tm_mon - 1) / 4]
term2 = cscTerms[int(time.localtime().tm_mon - 1) / 4]
members = cscLdap.search_s(cscPeopleBase, ldap.SCOPE_SUBTREE,
          '(&(objectClass=member)(term=%s%d))' % (term, year))
members.sort(key=lambda (_, y): y['cn'][0])
print "<h1>Members This Term</h1>"
print "<p>The members for " + term2 + " " + str(year) + " are listed here. We currently have " + str(len(members)) + " members.<br>"
print "Use of this list for solicitation of any form is prohibited. If you wish to get in touch with the membership as a whole please contact <a href=\"mailto:exec@csclub.uwaterloo.ca\">the Executive</a>.</p>"
print "<center>"
print "<table>"
print "    <tr><th>Name</th><th>Program</th><th>Username</th>"
for (_, member) in members:
    if not 'program' in member:
        member['program'] = ['']
    print "    <tr>"
    print "        <td><a href=\"" + member['uid'][0] + "\">" + member['cn'][0] + "</a></td>"
    print "        <td>" + member['program'][0] + "</td>"
    print "        <td>" + member['uid'][0] + "</td>"
    print "    </tr>"
print "</table>"
print "</center>"
