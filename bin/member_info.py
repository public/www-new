#!/usr/bin/python
import sys, os.path, ldap, time
uid = os.path.basename(sys.argv[1])

cscUri = "http://csclub.uwaterloo.ca/xsltproc"
cscTerms = ["Winter", "Spring", "Fall"]
cscShortTerms = ['w', 's', 'f']
cscLdapUri = "ldap://ldap1.csclub.uwaterloo.ca ldap://ldap2.csclub.uwaterloo.ca"
cscLdap = None
cscPeopleBase = 'ou=People,dc=csclub,dc=uwaterloo,dc=ca'

def cscLdapConnect():
    global cscLdap
    cscLdap = ldap.initialize(cscLdapUri)
    cscLdap.simple_bind_s("", "")

cscLdapConnect()
curDate = time.strftime('%d-%m-%Y')
members = cscLdap.search_s(cscPeopleBase, ldap.SCOPE_SUBTREE,
          '(&(objectClass=member)(uid=%s))' % uid)
if len(members) == 0:
    print "<p>No such user <code>" + uid + "</code></p>"
    sys.exit()   

(_, user) = members[0]

print "<h1>Profile for " + user['cn'][0] + "</h1>"

print "<p>"
print "<ul>"
if "position" in user:
    print "<li>Executive Position: "
    for x in user['position']:
        print x # edge case - could hold multiple positions!
    print "</li>"
print "<li>Terms: "
for t in sorted(user['term']):
    print t + " "
print "<br></li>"
print "<li>Website: <a href=\"http://csclub.uwaterloo.ca/~" + uid + "\">http://csclub.uwaterloo.ca/~" + uid + "</a></li>"
print "</ul>"
print "</p>"

