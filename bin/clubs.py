#!/usr/bin/python
import ldap, time, operator

#
# globals
#
cscUri = "http://csclub.uwaterloo.ca/xsltproc"
cscTerms = ["Winter", "Spring", "Fall"]
cscShortTerms = ['w', 's', 'f']
cscLdapUri = "ldap://ldap1.csclub.uwaterloo.ca ldap://ldap2.csclub.uwaterloo.ca"
cscLdap = None

cscClubBase = 'ou=People, dc=csclub, dc=uwaterloo, dc=ca'
cscGroupBase = 'ou=Group, dc=csclub, dc=uwaterloo, dc=ca'
cscPeopleBase = 'ou=People,dc=csclub,dc=uwaterloo,dc=ca'


def cscLdapConnect():
    global cscLdap
    cscLdap = ldap.initialize(cscLdapUri)
    cscLdap.simple_bind_s("", "")

cscLdapConnect()

curDate = time.strftime('%d-%m-%Y')
year = time.localtime().tm_year
term = cscShortTerms[int(time.localtime().tm_mon - 1) / 4]
term2 = cscTerms[int(time.localtime().tm_mon - 1) / 4]


clubs = cscLdap.search_s(cscClubBase, ldap.SCOPE_SUBTREE, '(&(objectClass=club))') 
groups = cscLdap.search_s(cscGroupBase, ldap.SCOPE_SUBTREE, '(&(objectClass=group))')
members = cscLdap.search_s(cscPeopleBase, ldap.SCOPE_SUBTREE,'(&(objectClass=member)(|(term=%s%d)(nonMemberTerm=%s%d)))' % (term, year, term, year))


clubs.sort(key=lambda (_, y): y['cn'][0])
print "<h1>Clubs This Term</h1>"
print "<center>"
print "<table>"
print "    <tr><th>Name</th></tr>"


def doshit(club):
    for (_, group) in groups:
        if group['cn'][0] == club['uid'][0] and group.has_key('uniqueMember'):
            for clubrep in group['uniqueMember']:
                clubrepname =  clubrep.split(",")[0][4:]
                for (_, member) in members:
                    if member['uid'][0] == clubrepname:
                        print "    <tr>"
                        print "        <td><a href=\"http://csclub.uwaterloo.ca/~" + club['uid'][0] + "\">" + club['cn'][0] + "</a></td>"
                        print "    </tr>"
                        return

for (_, club) in clubs:
  doshit(club)

print "</table>"
print "</center>"
