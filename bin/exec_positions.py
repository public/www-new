#!/usr/bin/python
import ldap, time, operator

#
# globals
#
cscUri = "http://csclub.uwaterloo.ca/xsltproc"
cscTerms = ["Winter", "Spring", "Fall"]
cscShortTerms = ['w', 's', 'f']
cscLdapUri = "ldap://ldap1.csclub.uwaterloo.ca ldap://ldap2.csclub.uwaterloo.ca"
cscLdap = None
cscPeopleBase = 'ou=People,dc=csclub,dc=uwaterloo,dc=ca'

def cscLdapConnect():
    global cscLdap
    cscLdap = ldap.initialize(cscLdapUri)
    cscLdap.simple_bind_s("", "")

cscLdapConnect()

def showExec(pretty, short):
    print "<p><b>" + pretty + "</b><br>"
    members = cscLdap.search_s(cscPeopleBase, ldap.SCOPE_SUBTREE,
             '(&(objectClass=member)(position=%s))' % short)
    if len(members) > 0:
      (_, user) = members[0]
      print user['cn'][0]
    else:
      print 'Seat Empty'
    print "</p>"

print "<h1>The Executive</h1>"
showExec("President", "president")
showExec("Vice-President", "vice-president")
showExec("Treasurer", "treasurer")
showExec("Secretary", "secretary")
showExec("Systems Administrator", "sysadmin")

print "<h1>Other Positions</h1>"
showExec("Chief Returning Officer", "cro")
showExec("Office Manager", "offsck")
showExec("Librarian", "librarian")

print "<h1>Past Executive</h1>"
print "<p>For a partial list of past executives, see <a href=\"http://wiki.csclub.uwaterloo.ca/Past_Executive\">the relevent wiki page</a></p>"

print "<h1>Elections</h1>"
print "<p>Each term the CSC holds elections to determine the executive council. To find out when and where the next elections will be held, check the homepage and the <a href=\"http://csclub.uwaterloo.ca/newsgroup/thread.php?group=uw.csc\">uw.csc</a> newsgroup.<br>"
print "</p><p>For details on the election rules, see the <a href=\"../Constitution\">constitution</a>."
print "</p>"
