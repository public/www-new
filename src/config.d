import std.regex;

const string url_root = "/~j3parker/";
const string site_title = "CSC";
const string site_subtitle = "UWaterloo Computer Science Club";

const bool nav_tree_vert = false;

string[StaticRegex!char] handlers;

void init_handlers() {
  handler!("(.*).md").add("contrib/Markdown.pl");
  handler!("^changelog$").add("changelog.sh");
  handler!("^About/Members/$").add("members.py");
  handler!("^About/Members/(.+)$").add("member_info.py");
  handler!("^About/Executive$").add("exec_positions.py");
  handler!("^About/Clubs$").add("clubs.py");
}

template handler(string pattern) {
  void add(string h) {
    try {
      handlers[ctRegex!(pattern)] = h;
    } catch (std.regex.Exception re) { return; }
  }
}
