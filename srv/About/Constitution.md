<h2>1. Name</h2>
The name of this organization shall be the "Computer Science Club
of the University of Waterloo".

<a id="purpose"></a><h2>2. Purpose</h2>

<ol><li>The Club is organized and will be operated exclusively for educational
and scientific purposes in furtherance of:
<ul>
<li>a) promoting an increased knowledge of computer science and its applications.</li>
<li>b) promoting a greater interest in computer science and
its applications; and</li>
<li>c) providing a means of communication between persons
having interest in computer science.</li>
</ul>
</li>

<li> the above purposes will be fulfilled by the organization of
discussions and lectures with professionals and academics in the field
of computer science and related fields, co-operation with the Student
Chapter of the ACM of the University of Waterloo, the maintenance of a
library of materials related to computer science, the maintenance of
an office containing the library as an aid to aim (<a href="#comm">1c</a>) above, and such other means as
decided by the club membership.</li>
</ol>

<a id="membership"></a><h2>3. Membership</h2>

<ol>

<li>In compliance with MathSoc regulations and in recognition of the club being primarily targeted at undergraduate students, full membership is open
to all Social Members of the Mathematics Society
and restricted to the same.</li>

<li> Affiliate membership in this Club shall be open to all other
persons.  Affiliate members shall have all the rights of full members
except for the rights of voting and holding executive office.</li>

<li> A person is not a member until he or she has paid
the current membership
fee and has been issued a membership card.
Membership lasts until the end of the term in which it was issued.
The membership card indicates, at least, a member's name, membership number,
the term for which the membership is valid, and an indication that
the member is an associate member, if indeed that is true.
The membership fee is set from time to time by the Executive.</li>
</ol>
<a id="officers"></a><h2>4. Officers</h2>

<ol><li> The officers of the Club shall be:
<ol><li> President</li>
<li> Vice-President</li>
<li> Secretary</li>
<li> Treasurer</li>
<li> System Administrator</li>

</ol></li>

<li> The faculty advisor shall be an ex-officio member of the
	Club.</li>

<li> The choice of officers shall be limited to full members of the
	Club.</li>

<li> All officers, other than the System Administrator, shall be
elected at a meeting to be held no later than three weeks after the
start of each school term, such start defined as the beginning of
lectures in the Winter and Spring terms, and as the beginning of
Orientation Week in the Fall term.</li>

<li> The election of officers shall be accomplished by the following
	procedure:

<ol>

<li> The past executive shall choose a willing Chief Returning
Officer, with duties defined below, set an election date, and set a
date for the opening of nominations such that the nomination period
(from the opening of nominations to 4:30 P.M.  on the day preceding
the elections) is at least one week.</li>

<li> Announcements of the election and the nomination procedure must
be distributed to all members by one or more of electronic mail,
Usenet, or posters in at least the Math and Computer building.</li>

<li> During the nomination period, the Chief Returning Officer (CRO)
shall be available to receive nominations for the posts of officers of
the club, either in person, by depositing nomination forms in the
CSC's mailbox in the MathSoc office, or by writing the nomination in a
place in the CSC office to be specified by the CRO.</li>

<li> A nomination shall consist of the nominee's name, and post(s)
nominated for.  Nominees must be full members of the Computer Science
Club.</li>

<li> The election shall commence with the offering of memberships for
sale.  After a reasonable time, control of the meeting is given to the
CRO who will manage the election of the President, Vice-President,
Treasurer, and Secretary, in that order.  Each election shall be
carried out by secret ballot, in a manner to be decided on by the CRO,
with the approval of the membership.  An accurate count of votes for
each candidate for each position must be made and recorded by the CRO.
A simple heads-down-hands-up method is considered acceptable.  As soon
as possible the CRO will announce the results of the election and the
winner will be removed from subsequent contests.  If this removal or
lack of nominations results in a vacant post, elections for such posts
may not be held at the election meeting, but elections for such posts
may be held at a later date, in accordance with the election
procedures.  The membership decides whether or not to hold extra
elections.</li>
</ol>
</li>

<li> Following the elections, it is the responsibility of the new executive
to select a System Administrator.  The selection of System Administrator must
then be ratified by the membership of the Club.  If a suitable System
Administrator is not available, then the executive may delay their selection
until one becomes available.  In this case the selection of System
Administrator must be ratified at the next meeting of the Club.</li>

<li> The faculty advisor shall be appointed by the present executive council
within three (3) weeks of each term's elections.  The faculty
advisor shall be a person having membership in the Association for Computing
Machinery and having a genuine interest in the Club.</li>

<li> Any two offices may be held by a single person if the President
deems this advisable, subject to the consent of the membership.</li>

<li> In the case of a resignation of an officer or officers, the membership
may call elections to replace such officer(s).
If they are called, they must follow the given election procedure.
Such elections would be for all offices which are vacant.</li>
</ol>
<a id="duties"></a><h2>5. Duties of Officers</h2>
<ol>

<li> The duties of the President shall be:

<ol>
<li> to call and preside at all general, special, and executive
meetings of the Club;</li>

<li> to appoint all committees of the Club and the committee chairmen
of such committees, except the chairman of the programmes committee;
and</li>

<li> to audit, or to appoint a representative to audit, the financial
records of the club at the end of each academic term.</li>

</ol>
</li>

<li> The duties of the Vice-President shall be:
<ol><li> to assume the duties of the President
in the event of the President's absence;</li>
<li> to act as chairman of the programmes committee; and</li>
<li> to assume those duties of the President
that are delegated to him by the President.</li>

</ol>
</li>

<li> The duties of the Secretary shall be:
<ol><li> to keep minutes of all Club meetings;</li>
<li> to prepare the annual Club report for
approval by the executive council;</li>
<li> to care for all Club correspondence;</li>
</ol>
</li>

<li> The duties of the Treasurer shall be:
<ol>
<li> to collect dues and maintain all financial and membership records;</li>
<li> to produce a financial or membership statement when requested;</li>
</ol>
</li>

<li> The duties of the System Administrator shall be:
<ol>
<li> to chair the Systems Committee</li>

<li> to perform in conjunction with the Systems Committee, the duties listed
in <a href="#sysduties">7.2</a>.</li>
</ol>
</li>
</ol>
<a id="executive"></a><h2>6. Executive Council</h2>
<ol>

<li> The executive council shall consist of the present officers of
the Club and the faculty advisor.  The new executive council shall
take effect immediately following the installation of new officers or
a new faculty advisor.</li>

<li> The executive council has the power of a general assembly, though
an actual general assembly may overrule its decisions.  Minutes of the
executive council meetings shall be available for inspection by any
member of the Club and shall be filed with the Club records.  Further,
all recent executive council decisions shall be announced at the next
regular meeting of the club.</li>

</ol>
<a id="committees"></a><h2>7. Committees</h2>
<h3>1. Programme Committee</h3>
<ol><li>The programme committee shall be a standing committee.</li>
<li> The programme committee shall plan and arrange meetings of the Club in
accordance with apparent membership interests and the aims of the
Club as set forth in Section <a href="#purpose">2</a>.</li>

<li> The programme committee shall be responsible to the executive
council.</li>
</ol>

<h3><a id="sysduties"></a>2. Systems Committee</h3>

<ol><li> The Systems Committee will be a standing committee, chaired by the 
Systems Administrator.</li>
<li> The Executive Council shall appoint members to the Systems Committee.
Such members should show interest and ability in Systems Administration.</li>
<li> The Systems Committee will collectively, under the leadership of the
Systems Administrator,
<ol><li> to operate any and all equipment in the possession of the Club.</li>
<li> to maintain and upgrade the software on equipment that is operated by
the Club.</li>

<li> to facilitate the use of equipment that is operated by the
	  Club.</li>

</ol></li></ol>

<h3>3. Library Committee</h3>

<ol><li> The library committee will be a standing committee, headed by the
Librarian of the Computer Science Club.</li>
<li> The Librarian shall be selected by the Executive Council.</li>
<li> Either the Executive Council or the Librarian may appoint members to
the library committee.</li>

<li> The library committee will be responsible for
<ol><li> purchasing new materials to be added to the Computer Science Club
library, with a target amount of money to spend indicated by the Treasurer;</li>
<li> keeping the library in some semblance of order;</li>
<li> in conjunction with the Systems Committee, maintaining an up-to-date,
online record of the materials in the library.</li>
</ol></li></ol>


<h3>4. Other Committees</h3>

<ol>
<li> The President, with approval of the executive council, may appoint such
temporary committees as is deemed necessary.</li>
</ol>
<a id="meetings"></a><h2>8. Meetings</h2>

<ol><li> General meetings of the Club shall
be called at times designated by the
executive council, at the recommendations of the programme committee.</li>
<li> Special meetings may be called at any time
deemed necessary by the executive council or
by the faculty advisor.</li>
<li> All members shall be notified at
least two days prior to a forthcoming meeting.
Electronic mail and/or posting to Usenet will be considered sufficient
notification, though other forms of notification are also encouraged.</li>

<li> The Club shall hold meetings only in places
that are open to all members
of the Club.</li>
</ol>
<a id="finances"></a><h2>9. Finances</h2>

<ol>

<li> The Treasurer shall present to the Club, before the end of each
academic term, a complete financial statement of the past period, and
a tentative budget for the following academic term.</li>

<li> The Treasurer shall present, as part of the tentative budget, a
recommended fee to be levied on the members in the following academic
period.</li>

<li> The signing officers shall be the Treasurer, and one of the
President or Vice-President.</li>

<li> At the end of each term, the President or his/her representative shall ensure
that the financial records are complete and accurate.</li>
</ol>
<a id="amendments"></a><h2>10. Amendments and Procedures</h2>
<ol><li> Amendments to the constitution shall be made in the following
manner:

<ol><li> the proposed amendment shall be announced to all members via
one or more of

<ol><li> a general meeting</li>

<li> electronic mail</li>
<li> Usenet</li>
</ol></li>
<li> the proposed amendment shall be made available for viewing by all
members in the Computer Science Club office;</li>
<li> A business meeting shall be held
at least seven (7) days after the announcement and no more than thirty (30) days after;</li>
<li> At this business meeting,
the amendment shall be read again and voted upon.
Two thirds of those present
and voting shall be required to carry the amendment;</li>
</ol></li>

<li> A quorum necessary for the conduct of business
is defined as fifteen (15) members
or 2/3 of the membership, whichever is smaller.</li>

<li> Rulings on any point of procedure
not included in this constitution shall be
made by the President with approval of the faculty advisor.</li>
<li> The executive council must bring before the members,
within three (3) weeks,
any motions with the
signatures of ten (10) members affixed to it.</li>
<li> For a normal motion to pass, a simple majority is required.
For motions of impeachment and motions calling for new elections, a 2/3
majority is required, as is seven (7) days notice of voting on such a motion.
In addition, if such a motion is defeated, it cannot be brought before the
Club again within sixty (60) days.</li>
</ol>
<a id="dissolution"></a><h2>11. Dissolution</h2>

<ol><li> In the event of dissolution of the Club,
all assets of the Club shall be
transferred to the Mathematics Society of the University of Waterloo.</li>
</ol>

<a id="resources"></a><h2>12. Use of Club Resources</h2>

<ol><li> All resources under control of the Club are to be used in accordance with
the aims of the Club.</li>

<li> The President and Vice-President are jointly responsible
for the proper use of all Club
resources, except that the entire executive is responsible for the main Club
account on the main Math Faculty Computing Facility computer.
Permission to use a resource is automatically granted to someone responsible
for that resource, and optionally as determined by unanimous vote of those
responsible for a resource.
Granting permission to someone to use a resource does not make that person
responsible for the proper use of the resource, although the user is, of
course, responsible to the person granting permission.</li>
</ol>
<a id="revision"></a><h2>Revision</h2>

<p>The constitution was last revised on 5 February 2004.</p>
