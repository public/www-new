Machines
=========

For more technical details on our machines, see our [Machine List](http://wiki.csclub.uwaterloo.ca/Machine_List) page.

All of our machines can be accessed via `ssh`.

<center>
<table>
  <tr><th>Hostname</th><th>Notes</th><th>General Use?</th></tr>
  <tr><td>bit-shifter</td><td>Office terminal</td><td>✓</td></tr>
  <tr><td>strombola</td><td>Office terminal</td><td>✓</td></tr>
  <tr><td>maltodextrin</td><td>Office terminal</td><td>✓</td></tr>
  <tr><td>natural-flavours</td><td>Office terminal</td><td>✓</td></tr>
  <tr><td>nullsleep</td><td>Office terminal</td><td>✓</td></tr>
  <tr><td>gwem</td><td>Office terminal</td><td>✓</td></tr>
  <tr><td>caffeine (or csc/csclub)</td><td>Web/mail/etc. (is a container)</td><td>✓</td></tr>
  <tr><td>corn-syrup</td><td>Containers, heavy computations.</td><td>✓</td></tr>
  <tr><td>hfcs</td><td>TBA</td><td>✓</td></tr>
  <tr><td>denardo</td><td>UltraSparc</td><td>✓</td></tr>
  <tr><td>taurine</td><td>Preferred location for screen sessions</td><td>✓</td></tr>
  <tr><td>rainbowdragoneyes</td><td>MIPS</td><td>✓</td></tr>
  <tr><td>artificial-flavours</td><td>Backup server</td><td>✗</td></tr>
  <tr><td>ginseng</td><td>File server</td><td>✗</td></tr>
  <tr><td>glomag</td><td>Hosts the caffiene container</td><td>✗</td></tr>
</table>
</center>


Gadgets
========
Webcam Counter
-------
foo
Computer-Controlled Lightswitches
-------
bar
Robot Arm (Chess Robot)
-------
baz
