## About the Office

The CSC Office is located at room MC3036/3037, in the Math & Computer Building of the University of Waterloo. The office is open whenever office staff are around to keep it open, which basically means it is open most of the time. You can check by taking a look through our [web-enabled camera](../Webcams). At the office we have lots of books, a few computer terminals, and most of the time an array of knowledgeable people to talk to and ask questions.

One of our most popular services at the office is providing anybody with free CD/DVD copies of Free Software and Open Source operating system distributions.

Another favourite is our $0.50 pop for members. We have a fridge in the office which is at most times stocked with many different kinds of pop cans.

We can always use good office staff, so if you're interested in helping out, just come by the office and chat to somebody there, and the office manager will probably give you something to do.

Our office phone number is (519) 888-4567 x33870, and you can mail us at the following address.

    Computer Science Club
    Math & Computer 3036/3037
    University of Waterloo
    200 University Avenue West
    Waterloo, ON  N2L 3G1
    Canada
