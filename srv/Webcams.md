Webcams
======
Spy on the office, see whether it's open or who is passing by the door!

Maltodextrin
-----
<center>
<img src="http://maltodextrin.csclub.uwaterloo.ca:8081" width="640" height="480" title="maltodextrin's Webcam"/>
</center>

Bit-Shifter
-----
<center>
<img src="http://bit-shifter.csclub.uwaterloo.ca:8081/webcam.jpg" width="640" height="480" title="bit-shifter's Webcam"/>
</center>
